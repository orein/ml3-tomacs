library(plyr)
library(readr)
library(ggplot2)

# Change this path to point to the directory "config-0" created by the simulation:
setwd("<path of config-0>")

files <- list.files(pattern="run\\-\\d+\\.csv$", full.names=TRUE)

dataset <- ldply(files, read_csv)
dataset$age <- lapply(dataset$age, function(s) strsplit(s, "[,()]")[[1]][3])
dataset$age <- as.numeric(dataset$age) 

ggplot(data=dataset, aes(x=age)) + 
  geom_histogram(aes(y = stat(count) / sum(count)), breaks=seq(0, 90, by=1)) + 
  labs(x="age", y="relative frequency") +
  theme(text=element_text(size=8,  family="serif"))

ggsave(filename = "plot.png", width = 151.129423745, height = 50, units = "mm")