Software requirements:
- Java Runtime Environment 8 or newer (e.g., from https://adoptopenjdk.net/).
- R, with the packages "plyr", "readr" and "ggplot2".
- All other required software will be downloaded automatically when the experiment is executed the first time.

To reproduce Figure 14:
- Execute run.bat (Windows) or run.sh (Linux). This will take multiple hours, and will probably not finish reasonably quickly on a laptop.
- The experiment will produce an output folder results-*timestamp*. It contains a folder "config-0" (for the one parameter configuration in the experiment), which contains a file "config.csv" with the parameter values and one csv file with the observed data for each run.
- Change the the paths in line 6 of plot.r to point to the "config-0" folder.
- Execute the R script to produce the figure in the "config-0" folder.