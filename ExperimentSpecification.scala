package org.sessl

import org.jamesii.ml3.experiment.init.JsonStateBuilder
import sessl._
import sessl.ml3._

// Basic setup of the experiment:
class MigrationExperiment extends Experiment with ParallelExecution with ParameterMaps {
  simulator = NextReactionMethod()
  parallelThreads = -1
  replications = 1
  initializeWith(() => new JsonStateBuilder("migration/initialstate2000.json"))
  startTime = 1982
  stopTime = 2050

  model = "migration/migration.ml3"

  set("e" <~ 2.71828182846)
  set("pi" <~ 3.14159265359)
  set("minFertilityAge" <~ 12)
  set("maxFertilityAge" <~ 49)
  set("ageOfAdulthood" <~ 16)
  set("ageOfRetirement" <~ 65)
  set("minMarriageAge" <~ 9)
  set("maxMarriageAge" <~ 60)
  set("meanMigrationStartAge" <~ 17)
  set("spouseAgeModifier" <~ -0.01301431)
  set("intercept" <~ -0.490129556)
  set("homeCountryGini" <~ 0.4)
  set("hostCountryGini" <~ 0.3)

  fromFile("migration/maleMortality.csv")()
  fromFile("migration/femaleMortality.csv")()
  fromFile("migration/fertility.csv")()
  fromFile("migration/income.csv")()
  fromFile("migration/ageDifferenceModifier.csv")()
  fromFile("migration/baseMarriageRate.csv")()
  fromFile("migration/borderEnforcement.csv")()
  fromFile("migration/disc.csv")()
}

// Experiment to produce the data for Figure 14:
object ExperimentRun extends App {
  val exp = new MigrationExperiment with TimeMeasurement with Observation with CSVOutput {
    stopTime = 2050
    replications = 20
	
    set("xi" <~ 1)
    set("zeta" <~ 100)
    set("alpha" <~ 1)
    set("beta" <~ 1)
    set("gamma" <~ 1) 
    set("rho" <~ 0.25)

    val migration = Change("Person", "migrationStage", "ego.migrationStage = 'migrated'")
    observe("age" ~ (expression("ego.age") at migration))
    observe("sex" ~ (expression("ego.sex") at migration))
    observe("cost" ~ (expression("ego.migrationCost()") at migration))
    observe("hh-capital" ~ (expression("ego.household.capital") at migration))
    observe("hh-income" ~ (expression("ego.household.members.sum(alter.income)") at migration))
    observe("hh-consumption" ~ (expression("ego.household.totalMonthlyConsumption()") at migration))
    observe("hh-size" ~ (expression("ego.household.members.size()") at migration))
    observe("hh-with-income" ~ (expression("ego.household.members.filter(alter.income > 0).size()") at migration))
    observe("hh-without-income" ~ (expression("ego.household.members.filter(alter.income = 0).size()") at migration))

    withRunResult(results => {
      writeCSV(results)
    })
  }
  execute(exp)
  println("Runtime: " + exp.executionTime.get.asInstanceOf[Double] / (60 * 1000) + " min");
}